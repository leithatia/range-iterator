package org.academiadecodigo.proxymorons.rangeiterator;

import java.util.Iterator;

public class RangeIterator implements Iterator<Integer> {
    private final int max;
    private int current;

    public RangeIterator(int min, int max) {
        this.max = max;
        current = min;
    }


    @Override
    public boolean hasNext() {
        return current <= max;
    }

    @Override
    public Integer next() {
        return current++;
    }

}
