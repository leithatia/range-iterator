package org.academiadecodigo.proxymorons.rangeiterator;

import java.util.Iterator;

public class Main {
    public static void main(String[] args) {

        Range range = new Range(1,10);

        for(Integer num : range) {
            System.out.println(num);
        }
    }
}
