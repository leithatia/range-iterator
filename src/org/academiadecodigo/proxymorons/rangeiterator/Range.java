package org.academiadecodigo.proxymorons.rangeiterator;

import java.util.Iterator;

public class Range implements Iterable<Integer> {
    private final int min;
    private final int max;

    public Range(int min, int max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new RangeIterator(min, max);
    }
}
